makeInsilicoAllelesFromVariants <-
  function(varTable, # data.frame with rows=genes, cols=inds
           seqIDcol = 1, # which column is the sequence ID in?
           #                  can be numeric or character
           refPosCol = "Reference.Position", # Which column has the reference position?
           #                                    can be numeric or character
           refAlleleCol = "Reference", # Which column has the reference allele?
           #                                    can be numeric or character
           varAlleleCol = "Allele", # Which column has the variable alleles?
           #                                    will be grepped, so make it unique enough
           readCutoffs = 1, # how many variable positions need to be present to calculate
           #                      set to 1 (or 0 or NULL) to include all
           #                      without a reference, small numbers will be almost meaningless
           referenceSeqs, # FASTA format to pull out the sequences
           maxReturn = 1 # Max number of alleles to return per gene
  ){

    if(is.null(readCutoffs)){
      readCutoffs <- 1
    } else if(!is.numeric(readCutoffs)){
      stop("ERROR: readCutoffs must be numeric or NULL")
    }
    
    
    ## Make all column inputs numeric
    
    if(is.character(seqIDcol)){
      seqIDcolOut <- which(colnames(varTable) == seqIDcol)
      if(length(seqIDcolOut) != 1){
        stop("ERROR: seqIDcol did not match exactly one column name")
      }
    } else if(is.numeric(seqIDcol)){
      seqIDcolOut <- seqIDcol
    } else{
      stop("ERROR: seqIDcol must be numeric or character")
    }
    
    if(is.character(refPosCol)){
      refPosColOut <- which(colnames(varTable) == refPosCol)
      if(length(refPosColOut) != 1){
        stop("ERROR: refPosCol did not match exactly one column name")
      }
    } else if(is.numeric(refPosCol)){
      refPosColOut <- refPosCol
    } else{
      stop("ERROR: refPosCol must be numeric or character")
    }
    
    ## Set ref position to numeric:
    varTable[,refPosColOut] <- as.numeric(as.character(varTable[,refPosColOut]))
    
    
    if(is.character(refAlleleCol)){
      refAlleleColOut <- which(colnames(varTable) == refAlleleCol)
      if(length(refAlleleColOut) != 1){
        stop("ERROR: refAlleleCol did not match exactly one column name")
      }
    } else if(is.numeric(refAlleleCol)){
      refAlleleColOut <- refAlleleCol
    } else{
      stop("ERROR: refAlleleCol must be numeric or character")
    }
    
    
    if(is.character(varAlleleCol)){
      varAlleleColOut <- which(colnames(varTable) == varAlleleCol)
      if(length(refAlleleColOut) != 1){
        stop("ERROR: varAlleleCol did not match exactly one column name")
      }
    } else if(is.numeric(varAlleleCol)){
      varAlleleColOut <- varAlleleCol
    } else{
      stop("ERROR: varAlleleCol must be numeric or character")
    }
    
    
    # Save limited set
    varTableLim <- varTable[,c(seqIDcolOut,refPosColOut,refAlleleColOut,varAlleleColOut)]
    
    
    # Convert variants to character
    varTableLim[,3] <- as.character(varTableLim[,3])
    varTableLim[,4] <- as.character(varTableLim[,4])
    
    
    ## Split the table by seqID, keep only the reference positions
    splitVars <- split(varTableLim,varTableLim[,1])
    
    nVarPos <- unlist(lapply(splitVars,function(x) length(x[,1])))
    limVars <- splitVars[nVarPos >= readCutoffs]
    
      ## "cds" assumes that everything is in frame
      
      ## For testing
      # limVars <- limVars[1:100]
      # x <- limVars[[1]] ## For testing
      # x <- limVars[["Beilschmiedia_comp124531_c0_seq10"]]
      
      allVariants <- list()
      for(j in names(limVars)){ 
          x <- limVars[[j]]
          whichSeq <- as.character(x[1,1])
          seq <- referenceSeqs[[whichSeq]] 
          # cat(whichSeq,";") ## For diagnosing problems
          # grabs the sequence name, and pulls the matching fasta
          
          mults <- grep(",",x$Var)
          splitVar <- strsplit(x$Var,",")
          nVar <- unlist(lapply(splitVar,length))
          

          for(k in 1:min(max(nVar),maxReturn)){
            tempSeq <- seq
            tempSeq[x$Position] <- unlist(lapply(splitVar,function(y) {y[min(k,length(y))]}))
            
            allVariants[[paste(whichSeq,"_variant",k,sep="")]] <- paste(tempSeq,collapse="")
          }
          
          # write.fasta(synthSeqs,names(synthSeqs),"testFAout.fa",open = "a")
  
        } ## close the for loop
      
#       out <- do.call(rbind,byGeneFullInfo)
#       out <- list(variantInfo=variantInfo,geneInfo=geneInfo)
      
      return(allVariants)
      
    
  }
